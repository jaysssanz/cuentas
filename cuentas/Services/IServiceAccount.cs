﻿using cuentas.Models;

namespace cuentas.Services
{
    public interface IServiceAccount
    {
        Task<IEnumerable<Account>> GetAll();
        Task<Account> GetById(string Identificacion);
        Task<bool> Save(Account account);
        Task<bool> Update(Account account);
        Task<bool> Delete(string Identificacion);
    }
}
