﻿using cuentas.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cuentas.Services
{
    public class ServiceAccount : IServiceAccount
    {
        //Inyecccion de dependencia
        private readonly Contexto _contexto;
        private readonly IConfiguration _configuration;

        public ServiceAccount(Contexto contexto, IConfiguration configuration)
        {
            _configuration = configuration;
            _contexto = contexto;
        }
        //------------------------------------------------------------------------

        public async Task<IEnumerable<Account>> GetAll()
        {
            return await _contexto.Set<Account>().ToListAsync();
        }

        public async Task<Account> GetById(string Identificacion)
        {
            return await _contexto.Set<Account>().FirstOrDefaultAsync(a => a.Identificacion == Identificacion);
        }

        public async Task<bool> Save(Account account)
        {
            try
            {
                _contexto.Set<Account>().Add(account);
                await _contexto.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Update(Account account)
        {
            try
            {
                _contexto.Entry(account).State = EntityState.Modified;
                await _contexto.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(string Identificacion)
        {
            try
            {
                var account = await GetById(Identificacion);
                if (account != null)
                {
                    _contexto.Set<Account>().Remove(account);
                    await _contexto.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
