﻿using System.ComponentModel.DataAnnotations;

namespace cuentas.Models
{
    public class Account
    {
        [Key]
        
        public string Identificacion { get; set; } = null!;
        public decimal Saldo { get; set; }
        public string TipoDeCuenta { get; set; } = null!;
        public string NumeroDeCuenta { get; set; } = null!;
        public DateTime FechaDeApertura { get; set; }

    }
}
