﻿using Microsoft.EntityFrameworkCore;

namespace cuentas.Models
{
    public class Contexto : DbContext
    {
        public Contexto()
        {

        }

        public Contexto(DbContextOptions<Contexto> options)
            : base(options)
        {

        }

        public DbSet<Account> Account { get; set; }
        
    }
}
